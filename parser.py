# MMSI,BaseDateTime,LAT,LON,SOG,COG,Heading,VesselName,IMO,CallSign,VesselType,Status,Length,Width,Draft,Cargo,TranscieverClass
fn = "2018/AIS_2018_01_01.csv"
bound = [40.33, 40.77, -74.30, -73.50] # LAT(min, max), LON(min, max)
time_step = 60*10 #sec




from utils import *

all_df = pd.read_csv(fn)
df = all_df[ in_bound(all_df,bound) ]
ID = pd.unique(df["MMSI"])

df = df.sort_values('BaseDateTime')
N = len(ID)
t0 = np.datetime64(df['BaseDateTime'].values[0])
T = np.arange(t0,t0+60*60*24,10)

for i,t in enumerate(T):
    print(f"({i}){t}sec ",end='')
    dfi = df[ in_time(df,t,time_step) ]
    for j,id in enumerate(ID):
        print(f"{j}/{N}",end='\r')
        dfij = dfi[dfi["MMSI"]==id]
        plt.plot(dfij["LON"],dfij["LAT"],'.-')
    plt.axis("equal")
    plt.show()










#
