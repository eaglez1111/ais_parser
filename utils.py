import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def in_bound(df,b):
    return ( (df["LAT"]>b[0]) & (df["LAT"]<b[1]) & (df["LON"]>b[2]) & (df["LON"]<b[3]) )

def in_time(df,t,step):
    t0, t1 = str(t), str(t+step)
    return ( (df["BaseDateTime"]>=t0) & (df["BaseDateTime"]<=t1) )
